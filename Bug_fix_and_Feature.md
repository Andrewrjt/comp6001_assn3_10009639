#Bug Fix#

For my bug fix I went with the scaling issue with some of the assests, more specificly somethings of the same model were very much out of proportion chairs in reagans room looked like they were for giants as an example. 
This bug fix was very simple in that all I had to do was uniform all the assests to once scale. I choose tonys area as the base for the sizing of everything as his was the closest to real life. 
However his mice were sized incorrectly so I changed those. 
Then using everything from Tonys area to populate the areas I aslo reorganised the rooms object folders as none were parented to any logical ordering leaving 70+ assests in one object. 
Same scale is very important in VR programs as the sense of reality is lost if everything is in complete different poportions.


#Feature#

The feature I added was a new menu for the teleport UI feature in our program. I was the one that intially made it and I wasnt happy with how it worked. 
It was counter intuitive as the menu was clunky and you had to use the right controller to make your selection from a menu resting on the left controller, this goes against general practices found for most VR GUI. 
So the new menu is a radial menu that is activated by touching the left touchpad your options then appear just about the touchpad using the touchpad to make the selection then clicking down on the touchpad to choose, nothing is selected using anything else than 1 thumb (of finger if you hold the controller funny). 
This works now inline with how most modern VR projects bring up a personal menu for navagation.
